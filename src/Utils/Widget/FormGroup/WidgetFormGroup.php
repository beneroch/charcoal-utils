<?php

namespace Utils\Widget\FormGroup;

// From Pimple
use Pimple\Container;

// From 'charcoal-app'
use Charcoal\App\Template\WidgetInterface;

// From 'charcoal-admin'
use Charcoal\Admin\Widget\FormGroupWidget;

// From 'charcoal-ui'
use Charcoal\Ui\FormGroup\AbstractFormGroup;

/**
 *
 */
class WidgetFormGroup extends AbstractFormGroup
{
    /**
     * @var string
     */
    protected $widgetId;

    /**
     * @var string
     */
    protected $widgetType;

    /**
     * @var FactoryInterface
     */
    protected $widgetFactory;

    /**
     * @var WidgetInterface
     */
    protected $widget;

    /**
     * @var array
     */
    protected $renderableData = [];

    /**
     * Whether notes shoudl be display before or after the form fields.
     *
     * @var boolean
     */
    private $showNotesAbove = false;

    /**
     * @param  Container $container The DI container.
     * @return void
     */
    public function setDependencies(Container $container)
    {
        parent::setDependencies($container);

        $this->widgetFactory = $container['widget/factory'];

        // Satisfies ViewableInterface dependencies
        $this->setView($container['view']);
    }

    /**
     * @param  string $type The widget type.
     * @return WidgetFormGroup Chainable
     */
    public function setWidgetType($type)
    {
        $this->widgetType = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function widgetType()
    {
        return $this->widgetType;
    }

    /**
     * @return WidgetInterface|boolean
     */
    public function widget()
    {
        if ($this->widget) {
            return $this->widget;
        }

        $widgetType = $this->widgetType();

        if (!$widgetType) {
            return false;
        }

        $widget = $this->widgetFactory->create($widgetType);
        $widget->setData($this->data());
        $widget->setData($this->renderableData());
        $widget->setType($widgetType);
        $this->setDynamicTemplate('widget_template', $widgetType);
        
        return $widget;
    }

    /**
     * @return string
     */
    public function widgetId()
    {
        if (!$this->widgetId) {
            $this->widgetId = uniqid();
        }

        return $this->widgetId;
    }

    /**
     * @param  array $data The data to render.
     * @return self
     */
    public function setRenderableData(array $data)
    {
        $this->renderableData = $this->renderRecursiveData($data);

        return $this;
    }

    /**
     * @return mixed
     */
    public function renderableData()
    {
        return $this->renderableData;
    }

    /**
     * @param  array|\Traversable $data The data to render.
     * @return array|\Traversable The rendered data.
     */
    private function renderRecursiveData($data)
    {
        foreach ($data as $key => $val) {
            if (is_string($val)) {
                $data[$key] = $this->renderData($val);
            } elseif (is_array($val)) {
                $data[$key] = $this->renderRecursiveData($val);
            } else {
                continue;
            }
        }

        return $data;
    }

    /**
     * @param  string $data The data to render.
     * @return string The rendered data.
     */
    private function renderData($data)
    {
        $obj = $this->form->obj();

        // Make sure there's an "out"
        $out = $data;

        if ($obj instanceof ViewableInterface && $obj->view() !== null) {
            $out = $obj->view()->render($data, $obj->viewController());
        } else {
            $cb = function ($matches) use ($obj) {
                $method = trim($matches[1]);
                if (method_exists($obj, $method)) {
                    return call_user_func([$obj, $method]);
                } elseif (isset($obj[$method])) {
                    return $obj[$method];
                } else {
                    return '';
                }
            };

            $out = preg_replace_callback('~{{(.*?)}}~i', $cb, $data);
        }

        return $out;
    }

    /**
     * @return Translation|string|null
     */
    public function description()
    {
        return $this->renderTemplate((string)parent::description());
    }

    /**
     * @return Translation|string|null
     */
    public function notes()
    {
        return $this->renderTemplate((string)parent::notes());
    }

    /**
     * Show/hide the widget's notes.
     *
     * @param  boolean|string $show Whether to show or hide notes.
     * @return FormGroupWidget Chainable
     */
    public function setShowNotes($show)
    {
        $this->showNotesAbove = ($show === 'above');

        return parent::setShowNotes($show);
    }

    /**
     * @return boolean
     */
    public function showNotesAbove()
    {
        return $this->showNotesAbove;
    }
}
