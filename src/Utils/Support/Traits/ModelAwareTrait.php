<?php

namespace Utils\Support\Traits;

use Exception;

// From Pimple
use Pimple\Container;

// From 'charcoal-core'
use Charcoal\Model\ModelInterface;
use Charcoal\Loader\CollectionLoader;

// From 'charcoal-factory'
use Charcoal\Factory\FactoryInterface;

/**
 *
 */
trait ModelAwareTrait
{
    /**
     * ModelFactory
     *
     * @var FactoryInterface
     */
    protected $modelFactory;

    /**
     * Protos to be kept in an associative array
     * @var array $proto
     */
    protected $proto = [];

    /**
     * Collection loader
     *
     * @var collectionloader
     */
    protected $loader;

    /**
     * @param  FactoryInterface $factory The factory to create models.
     * @return ModelAwareTrait
     */
    public function setModelFactory(FactoryInterface $factory)
    {
        $this->modelFactory = $factory;
        return $this;
    }

    /**
     * @throws Exception If the model factory is missing.
     * @return FactoryInterface
     */
    public function modelFactory()
    {
        if (!$this->modelFactory) {
            throw new Exception(sprintf(
                'Model Factory is missing for [%s]',
                get_called_class()
            ));
        }
        return $this->modelFactory;
    }

    /**
     * Returns a model prototype
     * Not to be used when calling multiple object
     * instances.
     *
     * @param  string $objType The object type to prototype.
     * @return ModelInterface
     */
    public function proto($objType)
    {
        if (isset($this->proto[$objType])) {
            return $this->proto[$objType];
        }
        $this->proto[$objType] = $this->obj($objType);

        return $this->proto[$objType];
    }

    /**
     * Return new instance of objType no matter what
     *
     * @param  string $objType The object type create.
     * @return ModelInterface
     */
    public function obj($objType)
    {
        $factory = $this->modelFactory();
        $obj = $factory->create($objType);
        return $obj;
    }

    /**
     * @param  string $objType The object type to collect.
     * @return CollectionLoader
     */
    public function collection($objType)
    {
        $obj = $this->obj($objType);
        $loader = new CollectionLoader([
            'logger'  => $this->logger,
            'factory' => $this->modelFactory()
        ]);
        $loader->setModel($obj);
        return $loader;
    }
}
