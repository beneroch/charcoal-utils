<?php

namespace Utils\Support\Traits;

// From Pimple
use Pimple\Container;

// From 'charcoal-utils'
use Utils\Helper\Helper;

/**
 *
 */
trait HelperAwareTrait
{
    /**
     * @var Helper
     */
    protected $helper;

    /**
     * @param  Helper $helper A utility belt.
     * @return HelperAwareTrait
     */
    public function setHelper(Helper $helper)
    {
        $this->helper = $helper;
        return $this;
    }

    /**
     * @return Helper
     */
    public function helper()
    {
        return $this->helper;
    }

    /**
     * @param Container $container The DI container.
     * @return void
     */
    abstract public function setDependencies(Container $container);
}
