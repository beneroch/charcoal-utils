<?php

namespace Utils\Support\Traits;

use Exception;

// From 'charcoal-factory'
use Charcoal\Factory\FactoryInterface;

/**
 * Get a widget easily.
 */
trait WidgetAwareTrait
{
    /**
     * @var FactoryInterface
     */
    private $widgetFactory;

    /**
     * @param  FactoryInterface $factory The factory create widgets.
     * @return WidgetAwareTrait
     */
    private function setWidgetFactory(FactoryInterface $factory)
    {
        $this->widgetFactory = $factory;
        return $this;
    }

    /**
     * @return FactoryInterface
     */
    private function widgetFactory()
    {
        return $this->widgetFactory;
    }
}
