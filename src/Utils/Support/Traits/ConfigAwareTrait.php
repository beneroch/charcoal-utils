<?php

namespace Utils\Support\Traits;

// From 'charcoal-app'
use Charcoal\App\AppConfig;

/**
 * Use in objects that needs the appConfig to work.
 * Appconfig is the global config of the project.
 *
 */
trait ConfigAwareTrait
{
    /**
     * @var AppConfig
     */
    protected $appConfig;

    /**
     * @param  AppConfig $config The application confiset.
     * @return ConfigAwareTrait Chainable
     */
    public function setAppConfig(AppConfig $config)
    {
        $this->appConfig = $config;
        return $this;
    }

    /**
     * @return AppConfig
     */
    public function appConfig()
    {
        return $this->appConfig;
    }
}
