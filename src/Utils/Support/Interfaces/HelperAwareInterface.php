<?php

namespace Utils\Support\Interfaces;

// From 'charcoal-utils'
use Utils\Helper\Helper;

/**
 *
 */
interface HelperAwareInterface
{
    /**
     * @param  Helper $helper A utility belt.
     * @return HelperAwareTrait
     */
    public function setHelper(Helper $helper);

    /**
     * @return Helper
     */
    public function helper();
}
