<?php

namespace Utils\Support\Interfaces;

// From 'charcoal-core'
use Charcoal\Model\ModelInterface;
use Charcoal\Loader\CollectionLoader;

// From 'charcoal-factory'
use Charcoal\Factory\FactoryInterface;

/**
 *
 */
interface ModelAwareInterface
{
    /**
     * @param  string $objType The object type to prototype.
     * @return ModelInterface
     */
    public function proto($objType);

    /**
     * @param  string $objType The object type create.
     * @return ModelInterface
     */
    public function obj($objType);

    /**
     * @param  string $objType The object type to collect.
     * @return CollectionLoader
     */
    public function collection($objType);
}
