<?php

namespace Utils\Support\Interfaces;

// From 'charcoal-app'
use Charcoal\App\AppConfig;

/**
 *
 */
interface ConfigAwareInterface
{
    /**
     * @param  AppConfig $config The application configset.
     * @return ConfigAwareTrait Chainable
     */
    public function setAppConfig(AppConfig $config);

    /**
     * @return AppConfig
     */
    public function appConfig();
}
