<?php

namespace Utils\Script;

use Exception;

// From Pimple
use Pimple\Container;

// From PSR-7
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

// From 'charcoal-app'
use Charcoal\App\Script\AbstractScript;

// From 'charcoal-utils'
use Utils\Support\Traits\ConfigAwareTrait;
use Utils\Support\Interfaces\ConfigAwareInterface;

use Utils\Support\Traits\HelperAwareTrait;
use Utils\Support\Interfaces\HelperAwareInterface;

use Utils\Support\Traits\ModelAwareTrait;
use Utils\Support\Interfaces\ModelAwareInterface;

/**
 * Find all strings to be translated in mustache or php files
 */
class BatchUpdateScript extends AbstractScript implements
    ConfigAwareInterface,
    HelperAwareInterface,
    ModelAwareInterface
{
    use ConfigAwareTrait;
    use HelperAwareTrait;
    use ModelAwareTrait;

    /**
     * @param  Container $container The DI container.
     * @return void
     */
    public function setDependencies(Container $container)
    {
        $this->setAppConfig($container['config']);
        $this->setModelFactory($container['model/factory']);
        // $this->setBaseUrl($container['request']->getUri()->getBaseUrl());
        $this->setHelper($container['utils/helper']);
        parent::setDependencies($container);
    }

    /**
     * Valid arguments:
     * - file : path/to/csv.csv
     *
     * @return array
     */
    public function defaultArguments()
    {
        $arguments = [
            'objtype' => [
                'longPrefix'   => 'obj-type',
                'description'  => 'What is the object type?',
                'defaultValue' => ''
            ]
        ];

        $arguments = array_merge(parent::defaultArguments(), $arguments);
        return $arguments;
    }

    /**
     * @param RequestInterface  $request  A PSR-7 compatible Request instance.
     * @param ResponseInterface $response A PSR-7 compatible Response instance.
     * @return ResponseInterface
     */
    public function run(RequestInterface $request, ResponseInterface $response)
    {
        // Unused
        unset($request);

        $climate = $this->climate();
        $objType = $this->argOrInput('objtype');

        $climate->underline()->out(
            'Batch updating '.$objType.'...'
        );

        $climate->out($objType);
        if (!$objType) {
            $climate->underline()->out('Missing Object Type');
            return $response;
        }

        $proto = $this->modelFactory()->create($objType);
        $loader = $this->collection($objType)->addFilter('active', true);

        $collection = $loader->load();
        foreach ($collection as $obj) {
            $obj->update();
            $climate->out('Updating obj id : '.$obj->id().' ... ');
        }


        return $response;
    }
}
