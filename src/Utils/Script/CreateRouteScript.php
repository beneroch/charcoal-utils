<?php

namespace Utils\Script;

use Exception;

// From Pimple
use Pimple\Container;

// From PSR-7
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

// From 'charcoal-app'
use Charcoal\App\Script\AbstractScript;

// From 'charcoal-object'
use Charcoal\Object\ObjectRoute;

// From 'charcoal-cms'
use Charcoal\Cms\TemplateableInterface;

// From 'charcoal-utils'
use Utils\Support\Traits\ModelAwareTrait;
use Utils\Support\Interfaces\ModelAwareInterface;

use Utils\Support\Traits\HelperAwareTrait;
use Utils\Support\Interfaces\HelperAwareInterface;

/**
 * Create a custom object route
 */
class CreateRouteScript extends AbstractScript implements
    HelperAwareInterface,
    ModelAwareInterface
{
    use HelperAwareTrait;
    use ModelAwareTrait;
    use TranslatorAwareTrait;

    /**
     * Object type for the route.
     * @var string $objType
     */
    private $objType;

    /**
     * Object ID for the route.
     * @var mixed $objId
     */
    private $objId;

    /**
     * Slug / URL of the route.
     * @var string $slug
     */
    private $slug;

    /**
     * Language of the route.
     * Set to '' for the route to be language agnostic
     * @var string $lang
     */
    private $lang;

    /**
     * @param  Container $container The DI container.
     * @throws Exception If the Helper object is missing.
     * @return void
     */
    public function setDependencies(Container $container)
    {
        $this->setModelFactory($container['model/factory']);
        $this->setTranslator($container['translator']);

        try {
            $this->setHelper($container['utils/helper']);
        } catch (Exception $e) {
            $this->climate()->error('Missing service providers');
            throw new Exception(sprintf(
                'Service "%s" not found. Add "%s" to your service providers.',
                'utils/helper',
                'utils/service-provider/utils'
            ));
        }

        parent::setDependencies($container);
    }

    /**
     * Arguments:
     * - --obj-type | -o Object type
     * - --obj-id   | -i Object ID
     * - --lang     | -l Language of the route (optional)
     * - --slug     | -s Actual wanted route for the object
     * @return array
     */
    public function defaultArguments()
    {
        $arguments = [
            'obj-type' => [
                'prefix'       => 'o',
                'longPrefix'   => 'obj-type',
                'description'  => 'What is the object type?'
            ],
            'obj-id' => [
                'prefix'       => 'i',
                'longPrefix'   => 'obj-id',
                'description'  => 'What is the object ID?'
            ],
            'lang' => [
                'prefix'       => 'l',
                'longPrefix'   => 'lang',
                'description'  => 'What is the route language?'
            ],
            'slug' => [
                'prefix'       => 's',
                'longPrefix'   => 'slug',
                'description'  => 'What is the desired slug?'
            ],
            'route-template' => [
                'prefix'       => 'r',
                'longPrefix'   => 'route-template',
                'description'  => 'What is the route template?'
            ]
        ];

        $arguments = array_merge(parent::defaultArguments(), $arguments);
        return $arguments;
    }

    /**
     * @param RequestInterface  $request  A PSR-7 compatible Request instance.
     * @param ResponseInterface $response A PSR-7 compatible Response instance.
     * @return ResponseInterface
     */
    public function run(RequestInterface $request, ResponseInterface $response)
    {
        // Unused
        unset($request);

        $climate = $this->climate();
        $climate->arguments->parse();

        $climate->green()->out(
            'Route creation script'
        );

        // Get argument values.
        $objType = $this->objType();
        $objId = $this->objId();
        $lang = $this->lang();

        $slug = $this->slug();
        // Should we sanitize it?
        $confirmSanitize = $climate->confirm(
            'Would you like the slug to be <red>sanitize</red> and <red>slugified</red>?'
        );
        if ($confirmSanitize->confirmed()) {
            $slug = $this->helper()->slugify($slug);
        }

        $objRoute = $this->modelFactory()->create(ObjectRoute::class);

        $objRoute->setRouteObjType($objType)
            ->setRouteObjId($objId)
            ->setLang($lang)
            ->setSlug($slug);

        $obj = $this->modelFactory()->create($objType)->load($objId);
        if (!$obj->id()) {
            $climate->red()->out('Object route create aborted: Invalid object ID.');
            return $response;
        }

        if ($obj instanceof TemplateableInterface) {
            $templateIdent = $obj->templateIdent();

            if ($templateIdent) {
                $confirmation = $climate->confirm(sprintf(
                    'Would you like to use a different template route/ident then : <red>%s</red>?',
                    $templateIdent
                ));
                if ($confirmation->confirmed()) {
                    // Ask for a new one.
                    $templateIdent = $this->routeTemplate();
                }
            }
        }

        // No ident?
        if (!isset($templateIdent) || !$templateIdent) {
            // Ask for one.
            $templateIdent = $this->routeTemplate();
        }

        $objRoute->setRouteTemplate($templateIdent);

        // NOTES:
        // isSlugUnique might affect the ID of the object.
        if (!$objRoute->isSlugUnique()) {
            $objRoute->generateUniqueSlug();
            $confirm = $climate->confirm(
                'This route is already used by another object. Do you still want to proceed?
                Route slug will be modified to :'.$objRoute->slug()
            );

            if (!$confirm->confirmed()) {
                $climate->red()->out('Object route creation aborted.');
                return $response;
            }
        }

        if ($objRoute->id()) {
            $confirmOverwrite = $climate->confirm(
                'The route '.$objRoute->slug().' was already defined for the same object. Do you
                wish to update the route with the provided datas?'
            );

            if ($confirmOverwrite->confirmed()) {
                $objRoute->update();

                $this->climate()->green()->out('Object route successfully updated.');
                $this->climate()->br();
                $this->climate()->red()->out((string)$objRoute->slug());
                return $response;
            }
        }

        $objRoute->save();
        $this->climate()->green()->out('Object route successfully created.');
        $this->climate()->br();
        $this->climate()->green()->out('Created object route slug : <red>'.(string)$objRoute->slug().'</red>');
        return $response;
    }

    /**
     * Route object type.
     * @return string Object type.
     */
    public function routeTemplate()
    {
        if ($this->routeTemplate) {
            return $this->routeTemplate;
        }

        $routeTemplate = $this->climate()->arguments->get('route-template');

        if (!$routeTemplate || !$this->isValidRouteTemplate($routeTemplate)) {
            $input = $this->climate()->input('What is the route template?');
            $that = $this;
            $input->accept(function ($response) use ($that) {
                return $that->isValidRouteTemplate($response);
            });

            $routeTemplate = $input->prompt();
        }
        $this->routeTemplate = $routeTemplate;
        return $this->routeTemplate;
    }

    /**
     * Route object type.
     * @return string Object type.
     */
    public function objType()
    {
        if ($this->objType) {
            return $this->objType;
        }

        $objType = $this->climate()->arguments->get('obj-type');

        if (!$objType || !$this->isValidObjType($objType)) {
            $input = $this->climate()->input('What is the object type?');
            $that = $this;
            $input->accept(function ($response) use ($that) {
                return $that->isValidObjType($response);
            });

            $objType = $input->prompt();
        }
        $this->objType = $objType;
        return $this->objType;
    }

    /**
     * Route object ID.
     * @return string The route object ID.
     */
    public function objId()
    {
        if ($this->objId) {
            return $this->objId;
        }

        $objId = $this->climate()->arguments->get('obj-id');

        if (!$objId || !$this->isValidObjId($objId)) {
            $input = $this->climate()->input('What is the object ID?');
            $that = $this;
            $input->accept(function ($response) use ($that) {
                return $that->isValidObjId($response);
            });

            $objId = $input->prompt();
        }
        $this->objId = $objId;
        return $this->objId;
    }

    /**
     * Route slug.
     * @return string Slug.
     */
    public function slug()
    {
        if ($this->slug) {
            return $this->slug;
        }

        $slug = $this->climate()->arguments->get('obj-id');

        if (!$slug || !$this->isValidSlug($slug)) {
            $input = $this->climate()->input('What is the desired route (slug)?');
            $that = $this;
            $input->accept(function ($response) use ($that) {
                return $that->isValidSlug($response);
            });

            $slug = $input->prompt();
        }
        $this->slug = $slug;
        return $this->slug;
    }

    /**
     * Route language.
     * @return string Language code | empty string.
     */
    public function lang()
    {
        if ($this->lang) {
            return $this->lang;
        }
        $lang = $this->climate()->arguments->get('obj-id');

        if (!$this->isValidLang($lang)) {
            $input = $this->climate()->input('What is the desired route language?');
            $that = $this;
            $input->accept(function ($response) use ($that) {
                return $that->isValidLang($response);
            });

            $lang = $input->prompt();
        }
        $this->lang = $lang;
        return $this->lang;
    }


    // ==========================================================================
    // UTILS
    // ==========================================================================
    /**
     * Validates the object type.
     * If the factory can't resolve the object type,
     * it is obviously an invalid one.
     * @todo Should verify if object is routable.
     * @param  string $routeTemplate A route template.
     * @return boolean Is Valid.
     */
    public function isValidRouteTemplate($routeTemplate)
    {
        // @todo check in template choices, etc.

        return (is_string($routeTemplate));
    }

    /**
     * Validates the object type.
     * If the factory can't resolve the object type,
     * it is obviously an invalid one.
     * @todo Should verify if object is routable.
     * @param  string $objType Object type.
     * @return boolean          Is Valid.
     */
    public function isValidObjType($objType)
    {
        try {
            $this->modelFactory()->get($objType);
        } catch (\Exception $e) {
            $this->error('Invalid object type argument!');
            return false;
        }

        return true;
    }

    /**
     * Validates the object ID.
     * @todo Should check if the object exists.
     * @param  string $id Object ID.
     * @return boolean     Is valid.
     */
    public function isValidObjId($id)
    {
        if (!$id) {
            return false;
        }
        return true;
    }

    /**
     * Validates the provided slug.
     * @param  string $slug Route slug.
     * @return boolean       Is Valid.
     */
    public function isValidSlug($slug)
    {
        return !!($slug);
    }

    /**
     * Validates the provided language with the available languages.
     * Can be empty, since GenericRoute should define the `currentLanguage`
     * when none provided.
     * @param  string $lang Language code.
     * @return boolean Is valid.
     */
    public function isValidLang($lang)
    {
        return (in_array($lang, $this->translator()->availableLocales()) || $lang === '');
    }

    /**
     * Display an error message
     * @param  string $msg Message to be displayed.
     * @return AbstractScript       Chainable
     */
    public function error($msg)
    {
        $this->climate()->error($msg);
        return $this;
    }
}
