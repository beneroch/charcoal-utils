<?php

namespace Utils\Script;

use Exception;
use UnexpectedValueException;
use ReflectionException;
use ReflectionMethod;
use JsonSerializable;
use Traversable;


// From Pimple
use Pimple\Container;

// From PSR-7
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

// From 'charcoal-core'
use Charcoal\Model\ModelInterface;
use Charcoal\Source\StorableInterface;

// From 'charcoal-app'
use Charcoal\App\Script\AbstractScript;

// From 'charcoal-utils'
use Utils\Support\Traits\ConfigAwareTrait;
use Utils\Support\Interfaces\ConfigAwareInterface;

use Utils\Support\Traits\HelperAwareTrait;
use Utils\Support\Interfaces\HelperAwareInterface;

use Utils\Support\Traits\ModelAwareTrait;
use Utils\Support\Interfaces\ModelAwareInterface;

/**
 * Find all strings to be translated in mustache or php files
 */
class BatchCommandScript extends AbstractScript implements
    ConfigAwareInterface,
    HelperAwareInterface,
    ModelAwareInterface
{
    use ConfigAwareTrait;
    use HelperAwareTrait;
    use ModelAwareTrait;

    /**
     * Inject dependencies from a DI Container.
     *
     * @param  Container $container A dependencies container instance.
     * @return void
     */
    public function setDependencies(Container $container)
    {
        parent::setDependencies($container);

        $this->setAppConfig($container['config']);
        $this->setModelFactory($container['model/factory']);
        $this->setHelper($container['utils/helper']);
    }

    /**
     * Retrieve the script's supported arguments.
     *
     * @return array
     */
    public function defaultArguments()
    {
        $arguments = [
            'obj-type' => [
                'prefix'       => 'o',
                'longPrefix'   => 'obj-type',
                'required'     => true,
                'description'  => 'What is the object type?'
            ],
            'func' => [
                'prefix'        => 'f',
                'longPrefix'    => 'function',
                'description'   => 'What function would you call?'
            ],
            'arguments' => [
                'prefix'        => 'a',
                'longPrefix'    => 'arguments',
                'description'   => 'Got any arguments you wanna pass?'
            ]
        ];

        $arguments = array_merge(parent::defaultArguments(), $arguments);
        return $arguments;
    }

    /**
     * Run the script.
     *
     * @param  RequestInterface  $request  A PSR-7 compatible Request instance.
     * @param  ResponseInterface $response A PSR-7 compatible Response instance.
     * @return ResponseInterface
     */
    public function run(RequestInterface $request, ResponseInterface $response)
    {
        unset($request);

        try {
            $this->start();
        } catch (Exception $e) {
            $this->climate()->error($e->getMessage());
        }

        return $response;
    }

    /**
     * Execute the prime directive.
     *
     * @todo   Implement {@see \Charcoal\App\CallableResolverAwareTrait} as per
     *     {@see Charcoal\Admin\Action\ElfinderConnectorAction}.
     * @return self
     */
    public function start()
    {
        $cli = $this->climate();

        $cli->br();
        $cli->bold()->underline()->out('Batch Command Execution');
        $cli->br();


        $objType = $this->argOrInput('obj-type');
        $func    = $this->argOrInput('func');
        $args    = $cli->arguments->get('arguments');

        if (!$objType) {
            $cli->error('Missing Object Type');
            return $this;
        }

        $loader = $this->collection($objType)->addFilter('active', true);
        $model  = $loader->model();

        $callable = $this->prepareCallable($func, $model);

        $cli->comment(
            sprintf(
                'Calling [<bold>%2$s</bold>] on [<bold>%1$s</bold>] objects.',
                $objType,
                $func
            )
        );

        $collection = $loader->load();
        $count      = count($collection);

        $resultMessage = 'Done';
        if ($count === 0) {
            $cli->error('No objects found');
            return $this;
        } elseif ($count === 1) {
            $resultMessage = 'Object was successfully processed';
            if (!$this->quiet()) {
                $cli->whisper('One object found');
            }
        } else {
            $resultMessage = 'Objects were successfully processed';
            if (!$this->quiet()) {
                $cli->whisper(sprintf('%s objects found', $count));
            }
        }

        if (!$this->quiet()) {
            # $progress = $cli->progress($count);

            $cli->whisper($count === 1 ? 'Processing the object…' : 'Processing objects…');
            $cli->br();
        }

        $output = [];
        foreach ($collection as $obj) {
            $objId = $obj->id();

            /*if (!$this->quiet()) {
                $progress->advance();
            }*/

            $callback = $this->resolveCallable($callable, $obj);
            if (isset($args)) {
                $result = call_user_func_array($callback, $args);
            } else {
                $result = call_user_func($callback);
            }

            if ($this->verbose()) {
                $result = $this->parseVar($result, $obj);
                $output/*[]*/ = [
                    sprintf('<dark_gray>%s</dark_gray>', $objId),
                    $result
                ];
                $cli->columns($output);
            }
        }

        $resultMessage = sprintf(
            '%d object%s successfully processed',
            $count,
            ($count > 1 ? 's were' : ' was')
        );

        if ($this->verbose()/* && $output*/) {
            # $cli->br();
            # $cli->columns($output);
            $cli->br();
        }

        $cli->info($resultMessage.'.');

        return $this;
    }

    /**
     * Prepare a callable variable.
     *
     * @param  callable|string $func  A callable function or method, either as a reference or string.
     * @param  object|null     $model Optional. An additional context under to test $callable as a method.
     * @return callable
     */
    protected function prepareCallable($func, $model = null)
    {
        $callable = $func;

        if (!is_callable($func) && is_string($func) && is_object($model)) {
            $callable = new ReflectionMethod($model, $func);
        }

        return $callable;
    }

    /**
     * Resolve a callable variable.
     *
     * @param  callable|string $callable A callable function or method, either as a reference or string.
     * @param  object|null     $obj      Optional. An additional context under to test $callable as a method.
     * @throws UnexpectedValueException If the given parameter is not callable.
     * @return callable
     */
    protected function resolveCallable($callable, $obj)
    {
        if (!$callable) {
            $callable = $obj;
        } elseif ($callable instanceof ReflectionMethod) {
            if ($callable->isStatic()) {
                $callable = $callable->getClosure();
            } else {
                return $callable->getClosure($obj);
            }
        } elseif (is_array($callable)) {
            $callable[0] = $obj;
        }

        if (!is_callable($callable)) {
            if (is_object($callable)) {
                $callable = get_class($callable);
            } elseif (is_array($callable)) {
                $callable = get_class($callable[0]).'::'.get_class($callable[1]);
            }

            throw new UnexpectedValueException(sprintf(
                '%1$s is not callable on %2$s',
                $callable,
                $obj->id()
            ));
        }

        return $callable;
    }

    /**
     * Parse the given variable into a string representation.
     *
     * @param  mixed       $var A variable.
     * @param  object|null $obj Optional. The related object.
     * @return string
     */
    protected function parseVar($var, $obj = null)
    {
        if ($var === null || (is_object($obj) && is_a($var, get_class($obj)))) {
            return null;
        }

        $var = $this->asScalar($var);

        if (is_array($var)) {
            $var = filter_var($var, \FILTER_CALLBACK, ['options' => [ $this, 'asScalar' ]]);
            return $this->prettyJson($var);
        }

        if (is_string($var) && $var[0] === '&') {
            return substr($var, 1);
        }

        ob_start();
        var_dump($var);
        return ob_get_clean();
    }

    /**
     * Parse the given variable into a string representation.
     *
     * @param  mixed $var A variable.
     * @return mixed
     */
    protected function asScalar($var)
    {
        if (is_scalar($var)) {
            return $var;
        }

        if (is_object($var)) {
            if ($var instanceof ModelInterface) {
                $str = sprintf('&%s', get_class($var));
                if ($var instanceof StorableInterface && $var->id()) {
                    $str .= sprintf(' #%s', $var->id());
                }
                $var = $str;
            } elseif (method_exists($var, 'toArray')) {
                $var = $var->toArray();
                # $var = $this->prettyJson($var->toArray());
            } elseif (method_exists($var, 'toJson')) {
                $var = json_decode($var->toJson(), true);
                # $var = $this->prettyJson($var->toJson());
            } elseif ($var instanceof JsonSerializable) {
                $var = $var->jsonSerialize();
                # $var = $this->prettyJson($var);
            } elseif ($var instanceof Traversable) {
                $var = iterator_to_array($var);
                # $var = $this->prettyJson(iterator_to_array($var));
            } elseif (method_exists($var, '__toString')) {
                $var = strval($var);
            } else {
                return '&'.get_class($var);
            }
        }

        return $var;
    }

    /**
     * Prettify JSON string.
     *
     * @param  string $json A JSON object.
     * @return string
     */
    protected function prettyJson($json)
    {
        $count = (is_array($json) ? count($json) : null);

        if ($count === 0) {
            return 'array(0) {}';
        }

        if (!is_string($json)) {
            $json = json_encode($json, JSON_UNESCAPED_UNICODE);
        }

        $json = preg_replace('#((?<!\\\\)[\{:,])#', '$1 ', $json);
        $json = preg_replace('#((?<!\\\\)\})#', ' $1', $json);

        return sprintf(
            'array(%d) %s',
            $count,
            $json
        );
    }
}
