<?php

namespace Utils\Object;

// From 'charcoal-core'
use Charcoal\Translator\Translation;

// From 'charcoal-object'
use Charcoal\Object\Content;

/**
 *
 */
class Text extends Content
{
    /**
     * @var string $ident
     */
    protected $ident;

    /**
     * Title of the text
     * l10n
     * @var Translation|string|null $title
     */
    protected $title;

    /**
     * Content of the text
     * tinymce/html
     * l10n
     * @var Translation|string|null $content
     */
    protected $content;

    /**
     * Image (if need be)
     * file
     * @var string $image
     */
    protected $image;

    /**
     * @return string
     */
    public function stripContent()
    {
        $content = $this->content();
        return strip_tags($content);
    }

/**
 * SETTERS
 */
    /**
     * @param  string $ident The ident.
     * @return Text
     */
    public function setIdent($ident)
    {
        $this->ident = $ident;
        return $this;
    }

    /**
     * @param  string $title The title.
     * @return Text
     */
    public function setTitle($title)
    {
        $this->title = $this->translator()->translation($title);
        return $this;
    }

    /**
     * @param  string $content The content.
     * @return Text
     */
    public function setContent($content)
    {
        $this->content = $this->translator()->translation($content);
        return $this;
    }

    /**
     * @param  string $image The path to the image.
     * @return Text
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }


/**
 * GETTERS
 */
    /**
     * @return string
     */
    public function ident()
    {
        return $this->ident;
    }

    /**
     * @return Translation|string|null
     */
    public function title()
    {
        return $this->title;
    }

    /**
     * @return Translation|string|null
     */
    public function content()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function image()
    {
        return $this->image;
    }
}
