<?php

namespace Utils\ServiceProvider;

// From Pimple
use Pimple\Container;
use Pimple\ServiceProviderInterface;

// From 'charcoal-core'
use Charcoal\Model\ModelLoader;

// From 'charcoal-utils'
use Utils\Helper\Helper;

/**
 * Email Service Provider
 */
class UtilsServiceProvider implements ServiceProviderInterface
{
    /**
     * @param Container $container A pimple container instance.
     * @return void
     */
    public function register(Container $container)
    {
        /**
         * @return ModelLoader
         */
        $container['utils/text/loader'] = function (Container $container) {
            return new ModelLoader([
                'obj_type' => 'utils/object/text',
                'obj_key'  => 'ident',
                'factory'  => $container['model/factory'],
                'cache'    => $container['cache']
            ]);
        };

        /**
         * @return Helper
         */
        $container['utils/helper'] = function (Container $container) {
            $instance = new Helper($container);
            return $instance;
        };
    }
}
