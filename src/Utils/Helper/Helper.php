<?php

namespace Utils\Helper;

// From Pimple
use Pimple\Container;

// From 'charcoal-app'
use Charcoal\App\Template\WidgetInterface;

// From 'charcoal-utils'
use Utils\Support\Traits\ModelAwareTrait;
use Utils\Support\Interfaces\ModelAwareInterface;

use Utils\Support\Traits\WidgetAwareTrait;
use Utils\Support\Interfaces\WidgetAwareInterface;

/**
 *
 */
class Helper implements
    ModelAwareInterface,
    WidgetAwareInterface
{
    use ModelAwareTrait;
    use WidgetAwareTrait;

    /**
     * @param  Container $container The DI container.
     * @return void
     */
    public function __construct(Container $container)
    {
        $this->setModelFactory($container['model/factory']);
        $this->setWidgetFactory($container['widget/factory']);
    }

    /**
     * Creates a widget with optional data.
     * @param  string     $ident Ident of the widget.
     * @param  array|null $data  Data to be injected to the template.
     * @throws Exception If the widget factory is missing.
     * @return WidgetInterface Actual requested widget.
     */
    public function createWidget($ident, $data = null)
    {
        if (!$this->widgetFactory()) {
            throw new Exception('Undefined WidgetFactory in '.get_class($this));
        }

        $widget = $this->widgetFactory()->create($ident);

        if ($data) {
            $widget->setData($data);
        }

        return $widget;
    }

    /**
     * Makes sure the URL is valid.
     * @param  string $url What need to be urlized.
     * @return string Urlized $url
     */
    public function urlize($url)
    {
        // Character options
        $separator = '_';
        $punctuation_modifier = $separator;

        // Punctuation
        $punctuation_characters = ['&', '%', '?', ')', '(', '\\', '"', "'", ':', '#', '.', ',', ';'];

        // Unescaped HTML characters string
        $unescaped_html_characters = '/&(raquo|laquo|rsaquo|lsaquo|rdquo|ldquo|rsquo|lsquo|hellip|amp|nbsp);/';

        $separator = '-';

        $url = mb_strtolower($url, 'UTF-8');

        // Strip HTML
        $url = strip_tags($url);

        // Strip Whitespace
        $url = trim($url);

        // Remove diacritics
        $url = preg_replace(
            '/&([a-zA-Z])(uml|acute|grave|circ|tilde|cedil|ring);/',
            '$1',
            htmlentities($url, ENT_COMPAT, 'UTF-8')
        );

        // Remove unescaped HTML characters
        $url = preg_replace($unescaped_html_characters, '', $url);

        // Get rid of punctuation
        $url = str_replace($punctuation_characters, $punctuation_modifier, $url);

        // Post-cleanup, get rid of spaces, repeating dash symbols symbols and surround whitespace/separators
        $url = trim($url);

        // Replace whitespace by seperator
        $url = preg_replace('/\s+/', $separator, $url);

        // Squeeze multiple dashes or underscores
        $url = preg_replace('/[-_]{2,}/', '-', $url);

        // Strip leading and trailing dashes or underscores
        $url = trim($url, '-_');

        // Finally, remove all whitespace
        $url = preg_replace('/[_]+/', $separator, $url);
        //$url = str_replace('_', $separator, $url);

        return $url;
    }

    /**
     * Creates a slug from a string.
     * @param string $str The string to slugify.
     * @return string The slugified string.
     */
    public function slugify($str)
    {
        // Do NOT remove forward slashes.
        $slug = preg_replace('/[^(\p{L}|\p{N})(\s|\/)]/u', '-', $str);

        $slug = mb_strtolower($slug, 'UTF-8');

        // Strip HTML
        $slug = strip_tags($slug);

        // Remove diacritics
        $slug = preg_replace(
            '/&([a-zA-Z])(uml|acute|grave|circ|tilde|cedil|ring);/',
            '$1',
            htmlentities($slug, ENT_COMPAT, 'UTF-8')
        );

        // Remove unescaped HTML characters
        $unescaped = '/&(raquo|laquo|rsaquo|lsaquo|rdquo|ldquo|rsquo|lsquo|hellip|amp|nbsp|quot|ordf|ordm);/';
        $slug = preg_replace($unescaped, '', $slug);

        // Replace whitespace by seperator
        $slug = preg_replace('/\s+/', '-', $slug);

        // Squeeze multiple dashes or underscores
        $slug = preg_replace('/[-]{2,}/', '-', $slug);

        // Strip leading and trailing dashes or underscores
        $slug = trim($slug, '-');

        // Finally, remove all whitespace
        $slug = preg_replace('/[_]+/', '-', $slug);

        return $slug;
    }

    /**
     * Like nl2br, but with paragraph tags.
     * @see http://stackoverflow.com/questions/5961217/how-do-i-surround-all-text-pieces-with-paragraph-tags
     * @param  string $str String with paragraphs.
     * @return string      String with P tags.
     */
    public function nl2p($str)
    {
        $arr   = explode("\n", $str);
        $out   = '';
        $count = count($arr);
        $i = 0;

        for (; $i < $count; $i++) {
            if (strlen(trim($arr[$i])) > 0) {
                $out .= '<p>'.trim($arr[$i]).'</p>';
            }
        }
        return $out;
    }
}
