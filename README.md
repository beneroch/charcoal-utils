Charcoal Utilities
==================

Support package providing various utilities for [Charcoal][charcoal-core] projects.

## Requirements

| Prerequisite     | How to check  | How to install |
| ---------------- | ------------- | -------------- |
| PHP >= 5.6.x     | `php -v`      | [php.net](//php.net/manual/en/install.php)
| Composer 1.0.0   | `composer -v` | [getcomposer.org](//getcomposer.org/)
| Charcoal 2016-12 |               |

## Installation

```shell
composer require beneroch/charcoal-utils
```

[charcoal-core]: https://github.com/locomotivemtl/charcoal-core
